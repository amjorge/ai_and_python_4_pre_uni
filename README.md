# Curso de ML para o secundário

Materiais para dar um curso muito curto de ML para o secundário.

Este material foi utilizado para divulgar a LIACD antes da sua primeira edição em julho de 2021. Participaram nessa primeira edição algumas equipas. Foi realizada um hackathon. Pelo menos uma das participantes (Bárbara L.) acabou por se inscrever na LIACD.

Contribuíram para esse evento:

- Carlos Soares
- Alípio Jorge
- Hugo Sousa
- Equipa do SICC da FCUP
